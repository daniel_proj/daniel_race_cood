/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myproject;

import java.sql.*;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class SqlServerConnection_1 {
     public Connection getConnection() throws Exception {
      String userName = "User_Game";
      String password = "123456";

            String url = "jdbc:sqlserver://DESKTOP-P4IBT5M;databaseName=NORTHWND";   
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn;
              conn = DriverManager.getConnection(url,userName,password);
              System.out.println("connection created");
              return conn;
              

    }   
     public ResultSet getResulset(String sql) throws Exception {
        Connection conn = getConnection();
        Statement st;
        st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        return rs;

    }
     
     public void Printtry_100(List<try_100> trying )
        throws SQLException, Exception {
        
         ResultSet rs = null;
        
        
       
              
         try {

             //Connect to DB
            SqlServerConnection_1 db = new SqlServerConnection_1();
             String PrintString = "select* from [Categories]";
            
             //Get  data from db
             rs = db.getResulset(PrintString);

            //Fill the LIst
            while (rs.next()) {
       try_100 item = new try_100();
       item.setCategoryID(rs.getInt("CategoryID"));
       System.out.print(rs.getInt("CategoryID"));
       item.setCategoryName(rs.getString("CategoryName"));
       System.out.print(rs.getString("CategoryName"));
       item.setDescription(rs.getString("Description"));
       System.out.print(rs.getString("Description"));
       item.setimage(rs.getBytes("Picture"));
       System.out.print(rs.getBytes("Picture"));
       trying.add(item);
       System.out.println(" ");
     }
         }
          catch(SQLException sqle)
        {
            System.out.println("Sql exception "+sqle);
        }
        finally {
             
            rs.close();
     }
     }
     public void updatetry_100(List<try_100> trying )
        throws SQLException, Exception {
         
        Connection con = null;
        PreparedStatement stUpdatetry_100  = null;
        
        String updateString =
            "update [Categories]"
                + " set [CategoryID] = ? "
                + ", [CategoryName] = ? "
                + ", [Description] = ? "
                + ", [Picture] = ?"; 
        
        try {
            con  = getConnection();

            stUpdatetry_100 = con.prepareStatement(updateString);
            
            for (try_100 item : trying) {
                
                stUpdatetry_100 .setInt(1,item.getCategoryID());
                stUpdatetry_100 .setString(2,item.getCategoryName());
                stUpdatetry_100 .setString(3,item.getDescription());
                stUpdatetry_100 .setBytes(4,item.getimage());
                
                
                stUpdatetry_100.executeUpdate();
            }
            
        }
        finally {
            if (stUpdatetry_100 != null){
                stUpdatetry_100.close();
            }
            
            if (con != null){
                if (!con.isClosed()){
                    con.close();
                }
            }
                
        }
        
    }
}
